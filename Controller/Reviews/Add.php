<?php

namespace Sanipex\Reviews\Controller\Reviews;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Add extends Action {

    public function __construct(
    Context $context, \Magento\Review\Model\ReviewFactory $reviewFactory, \Magento\Review\Model\RatingFactory $ratingFactory, \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_storeManager = $storeManager;
    }

    public function execute() {
//        echo "<pre>";print_r("Testing module Controller");die;
        $post = (array) $this->getRequest()->getPost();
        if (!empty($post)) {
            echo "<pre>";print_r($post);die;
            $productId = 1; //product id you set accordingly

            $reviewFinalData['ratings'][4] = 5;
            $reviewFinalData['nickname'] = "Usama habib";
            $reviewFinalData['title'] = "Create Review Programatically";
            $reviewFinalData['detail'] = "This is nice blog for magento 2.Creating product reviews programatically.";
            $review = $this->_reviewFactory->create()->setData($reviewFinalData);
            $review->unsetData('review_id');
            $review->setEntityId($review->getEntityIdByCode(\Magento\Review\Model\Review::ENTITY_PRODUCT_CODE))
                    ->setEntityPkValue($productId)
                    ->setStatusId(\Magento\Review\Model\Review::STATUS_PENDING)//By default set approved
                    ->setStoreId($this->_storeManager->getStore()->getId())
                    ->setStores([$this->_storeManager->getStore()->getId()])
                    ->save();

            foreach ($reviewFinalData['ratings'] as $ratingId => $optionId) {
                $this->_ratingFactory->create()
                        ->setRatingId($ratingId)
                        ->setReviewId($review->getId())
                        ->addOptionVote($optionId, $productId);
            }

            $review->aggregate();
            $this->messageManager->addSuccessMessage('Review done !');

            // Redirect to your form page (or anywhere you want...)
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/review/reviews/add');

            return $resultRedirect;
        }
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

}
